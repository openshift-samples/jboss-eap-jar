FROM registry.access.redhat.com/jboss-eap-7-tech-preview/eap-cd-openshift

ENV CUSTOM_INSTALL_DIRECTORIES /tmp/jboss/

EXPOSE 8080 8083

ADD helloworld.war /opt/eap/standalone/deployments
